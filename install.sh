#!/bin/bash

echo 'Installing Feral Image Conversion. Please wait.....'

echo 'Create ~/.local/share/kservices5'
mkdir ~/.local/share/kservices5

echo 'Create ~/.local/share/kservices5/ServiceMenus'
mkdir ~/.local/share/kservices5/ServiceMenus

echo 'Copy Applications to ~/.local/share/kservices5'
cp -r QMLReformat ~/.local/share/kservices5
cp -r QMLRotate ~/.local/share/kservices5
cp -r QMLResize ~/.local/share/kservices5

echo 'Ensure the executable files have the Execute flag set'
chmod +x ~/.local/share/kservices5/QMLReformat/reformat/reformat.py
chmod +x ~/.local/share/kservices5/QMLRotate/rotate/rotate.py
chmod +x ~/.local/share/kservices5/QMLResize/resize/resize.py

echo 'Copy the .desktop files to ~/.local/share/kservices5/ServiceMenus'
cp convertImages.desktop ~/.local/share/kservices5/ServiceMenus
cp rotateImages.desktop ~/.local/share/kservices5/ServiceMenus
cp resizeImages.desktop ~/.local/share/kservices5/ServiceMenus

echo 'Ensure the .desktop files have the Execute flag set'
chmod +x ~/.local/share/kservices5/ServiceMenus/convertImages.desktop
chmod +x ~/.local/share/kservices5/ServiceMenus/rotateImages.desktop
chmod +x ~/.local/share/kservices5/ServiceMenus/resizeImages.desktop

echo 'That should be it....'
