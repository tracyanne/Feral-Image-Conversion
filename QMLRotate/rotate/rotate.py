#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 27 08:32:00 2023

@author: tracy barlow
"""

# #This program is free software; you can redistribute it and/or modify it under
# #the terms of the GNU General Public License as published by
# #the Free Software Foundation; either version 2 of the License,
# #or (at your option) any later version.
# #
# #This program is distributed in the hope that it will be useful,
# #but WITHOUT ANY WARRANTY; without even the implied warranty
# #of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# #See the GNU General Public License for more details.
# #
# #You should have received a copy of the GNU General Public License along
# #with this program; if not, write to the Free Software Foundation, Inc.,
# #59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# This Python file uses the following encoding: utf-8
import sys
from pathlib import Path

from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import pyqtSignal, QObject, pyqtSlot, QUrl, Qt
from PyQt5.QtWidgets import QApplication

from wand.image import Image
import os


class Backend(QObject):

    setAppendText = pyqtSignal(str, arguments=['msg'])

    setProgressMax = pyqtSignal(float, arguments=['maxval'])
    updateProgress = pyqtSignal(int, arguments=['counter'])

    setSpinRotation = pyqtSignal(int, arguments=['val'])
    setSlideRotation = pyqtSignal(int, arguments=['val'])
    setRotationText = pyqtSignal(str, arguments=['msg'])
    setFilesMessage = pyqtSignal(str, arguments=['msg'])

    setImageProxyWidth = pyqtSignal(int, arguments=['width'])
    setImageProxyHeight = pyqtSignal(int, arguments=['height'])
    setImageProxyTop = pyqtSignal(int, arguments=['top'])
    setImageProxyLeft = pyqtSignal(int, arguments=['left'])

    setOverwrite = pyqtSignal(int, arguments=['checked'])

    setBusyIndicator = pyqtSignal(bool, arguments=['action'])

    closeApp = pyqtSignal()

    def __init__(self, images=[]):
        super().__init__()

        self.images = images

        self.rotation = 0
        self.overWrite = False
        self.appendText = ".rotated"

        self.tmpWidth = 0
        self.tmpHeight = 0
        self.nbrFiles = 0

        self.imageWidth = 0
        self.imageHeight = 0

    def loadValues(self):
        self.setBusyIndicator.emit(True)

        for filename in self.images:
            QApplication.processEvents()

            tmpimg = Image(filename=filename)
            self.tmpWidth += tmpimg.width
            self.tmpHeight += tmpimg.height
            self.nbrFiles += 1

        self.imageWidth = self.tmpWidth / self.nbrFiles
        self.imageHeight = self.tmpHeight / self.nbrFiles

        self.setSpinRotation.emit(self.rotation)
        self.setSlideRotation.emit(self.rotation)
        self.setRotationText.emit(str(self.rotation))

        self.setProgressMax.emit(self.nbrFiles)
        if self.nbrFiles > 1:
            self.setFilesMessage.emit("Rotating {0} Images".format(self.nbrFiles))
        else:
            self.setFilesMessage.emit("Rotating 1 Image")

        self.setAppendText.emit(self.appendText)
        self.setOverwrite.emit(self.overWrite)
        self.setBusyIndicator.emit(False)

        if self.imageWidth > self.imageHeight:
            self.setImageProxyWidth.emit(100)
            self.setImageProxyHeight.emit(60)
            self.setImageProxyTop.emit(30)
            self.setImageProxyLeft.emit(12)
        elif self.imageHeight > self.imageWidth:
            self.setImageProxyWidth.emit(60)
            self.setImageProxyHeight.emit(100)
            self.setImageProxyTop.emit(12)
            self.setImageProxyLeft.emit(30)
        else:
            self.setImageProxyWidth.emit(90)
            self.setImageProxyHeight.emit(90)
            self.setImageProxyTop.emit(18)
            self.setImageProxyLeft.emit(18)

    @pyqtSlot(int)
    def onSlideRotationChanged(self, rotation):
        self.rotation = rotation
        self.setSpinRotation.emit(self.rotation)
        self.setRotationText.emit(str(self.rotation))

    @pyqtSlot(int)
    def onSpinRotationChanged(self, rotation):
        self.rotation = rotation
        self.setSlideRotation.emit(self.rotation)
        self.setRotationText.emit(str(self.rotation))

    @pyqtSlot(str)
    def onTextRotationChanged(self, rotation):
        if int(rotation) < -180:
            self.setRotationText.emit(str(-180))
        elif int(rotation) > 180:
            self.setRotationText.emit(str(180))
        else:
            self.rotation = int(rotation)
            self.setSlideRotation.emit(self.rotation)
            self.setSpinRotation.emit(self.rotation)

    @pyqtSlot(bool)
    def onOverwriteToggled(self, checked):
        self.overWrite = checked
        # if checked:
        #     self.overWrite = checked
        # else:
        #     self.overWrite = Qt.Unchecked

    @pyqtSlot(str)
    def onTextChanged(self, text):
        self.appendText = text

    @pyqtSlot()
    def onProcess(self):
        self.rotateImages()

        self.closeApp.emit()

    def closeEvent(self, event):
        pass

    def rotateImages(self):
        cnt = 0

        for filename in self.images:
            QApplication.processEvents()

            tmpimg = Image(filename=filename)

            ptr = filename.rfind(".")
            filext = filename[ptr:]

            save_filename = filename[:(ptr)] + self.appendText + filext

            tmpimg.rotate(self.rotation)

            if self.overWrite:
                if Path(filename).is_file():
                    Path(filename).unlink()

                tmpimg.save(filename=filename)
            else:
                if Path(save_filename).is_file():
                    Path(save_filename).unlink()

                tmpimg.save(filename=save_filename)

            cnt += 1
            self.updateProgress.emit(cnt)


def main():
    app = QApplication(sys.argv)

    images = []
    try:
        images = sys.argv[1:]
    except:
        pass

    if len(images) > 0:

        app.setOrganizationName("Some Company")
        app.setOrganizationDomain("somecompany.com")
        app.setApplicationName("Amazing Application")

        engine = QQmlApplicationEngine()
        engine = QQmlApplicationEngine()

        engine.quit.connect(app.quit)

        qmlFile= os.path.dirname(os.path.realpath(__file__)) + "/main.qml"
        engine.load(qmlFile)

        # Define our backend object, which we pass to QML.
        backend = Backend(images)
        engine.rootObjects()[0].setProperty('backend', backend)
        backend.loadValues()
        sys.exit(app.exec_())


if __name__ == "__main__":
    main()



