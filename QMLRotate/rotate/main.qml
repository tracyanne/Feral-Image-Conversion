import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    id: rotateWindow
    width: 305
    height: 340
    visible: true
    title: qsTr("Rotate selected Images")
    flags: Qt.FramelessWindowHint | Qt.Window | Qt.Tool| Qt.WindowStaysOnTopHint | Qt.ApplicationModal | Qt.WindowModal

    property int spinrotationvalue
    property int sliderotationvalue
    property int proxyrotationvalue
    property string textrotationvalue

    property bool chkOverwritestate


    property QtObject backend

    BusyIndicator {
        id: busyIndicator
        x: 133
        y: 150
        z: 2
        visible: false
        running: false
    }

    Frame {
        id: frame
        x: 10
        y: 10
        width: 136
        height: 136

        Rectangle {
            id: imageProxy
            x: 17
            y: 37
            width: 100
            height: 60
            color: "#f70101"
            rotation: proxyrotationvalue
        }
    }

    Label {
        id: label1
        x: 166
        y: 10
        width: 129
        height: 102
        text: qsTr("Use Slider to change the angle of the Image, or use the spinner to enter an exact angle.")
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignTop
        wrapMode: Text.WordWrap
    }

    Label {
        id: label
        x: 166
        y: 115
        width: 58
        height: 32
        text: qsTr("Rotation")
        verticalAlignment: Text.AlignVCenter
    }

    SpinBox {
        id: spnRotation
        x: 224
        y: 115
        width: 71
        height: 32
        to: 180
        from: -180
        editable: true
        value: sliderotationvalue
        onValueModified: backend.onSpinRotationChanged(spnRotation.value)

        TextField {
            id: textRotation
            x: 0
            y: 0
            width: 50
            height: 32
            text: textrotationvalue
            z: 1
            validator: IntValidator{bottom: -180; top: 180;}
            onTextEdited: backend.onTextRotationChanged(textRotation.text)
        }
    }

    Slider {
        id: slideRotation
        x: 10
        y: 151
        width: 285
        height: 24
        stepSize: 1
        to: 180
        from: -180
        value: spinrotationvalue
        onMoved: backend.onSlideRotationChanged(slideRotation.value)
    }


    Label {
        id: label5
        x: 10
        y: 180
        width: 115
        height: 32
        visible: true
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Overwrite original")
    }

    CheckBox {
        id: chkOverwrite
        x: 125
        y: 180
        height: 32
        rotation: 0
        visible: true
        tristate: false
        checked: chkOverwritestate
        onToggled: backend.onOverwriteToggled(chkOverwrite.checked)
    }

    Label {
        id: label6
        x: 10
        y: 210
        width: 115
        height: 32
        visible: true
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Append with")
    }

    TextField {
        id: txtAppendText
        x: 125
        y: 210
        height: 32
        width: 120
        text: ""
        onTextEdited: backend.onTextChanged(txtAppendText.text)
    }

    Label {
        id: label2
        x: 10
        y: 240
        width: 285
        height: 32
        text: qsTr("Rotating 3000 Images")
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    ProgressBar {
        id: rotateProgress
        x: 10
        y: 270
        width: 285
        height: 24
        from: 0.0
        to: 10.0
        value: 0.0
    }

    Button {
        id: btnProcess
        x: 120
        y: 295
        text: qsTr("Rotate")
        onClicked: backend.onProcess()
    }

    Button {
        id: btnCancel
        x: 211
        y: 295
        text: qsTr("Cancel")
        onClicked: rotateWindow.close()
    }


    Connections {
        target: backend

        function onSetBusyIndicator(action) {
            busyIndicator.visible = action
            busyIndicator.running = action
        }

        function onCloseApp() {
            rotateWindow.close()
        }

        function onSetAppendText(msg) {
            txtAppendText.text = msg
        }

        function onSetSpinRotation(val) {
            sliderotationvalue = val
            proxyrotationvalue = val
        }

        function onSetSlideRotation(val) {
            spinrotationvalue = val
            proxyrotationvalue = val
        }

        function onSetRotationText(msg) {
            textRotation.clear();
            textrotationvalue = msg
        }

        function onSetFilesMessage(msg) {
            label2.text = msg
        }

        function onSetProgressMax(maxval) {
            rotateProgress.to = maxval
        }

        function onUpdateProgress(counter) {
            rotateProgress.value = counter
        }

        function onSetOverwrite(checked) {
            chkOverwritestate = checked
        }

        function onSetImageProxyWidth(width) {
            imageProxy.width = width
        }

        function onSetImageProxyHeight(height) {
            imageProxy.height = height
        }

        function onSetImageProxyTop(top) {
            imageProxy.y = top
        }

        function onSetImageProxyLeft(left) {
            imageProxy.x = left
        }
    }
}


