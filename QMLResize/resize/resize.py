#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 27 08:32:00 2023

@author: tracy barlow
"""

# #This program is free software; you can redistribute it and/or modify it under
# #the terms of the GNU General Public License as published by
# #the Free Software Foundation; either version 2 of the License,
# #or (at your option) any later version.
# #
# #This program is distributed in the hope that it will be useful,
# #but WITHOUT ANY WARRANTY; without even the implied warranty
# #of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# #See the GNU General Public License for more details.
# #
# #You should have received a copy of the GNU General Public License along
# #with this program; if not, write to the Free Software Foundation, Inc.,
# #59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# This Python file uses the following encoding: utf-8
import sys
from pathlib import Path

from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import pyqtSignal, QObject, pyqtSlot, QUrl, Qt
from PyQt5.QtWidgets import QApplication

from wand.image import Image
import os


class Backend(QObject):

    setAppendText = pyqtSignal(str, arguments=['msg'])
    setProgressMax = pyqtSignal(float, arguments=['maxval'])
    updateProgress = pyqtSignal(int, arguments=['counter'])
    setPercentage = pyqtSignal(int, arguments=['val'])
    setSpinWidth = pyqtSignal(int, arguments=['val'])
    setWidthText = pyqtSignal(str, arguments=['msg'])
    setInvalidWidth = pyqtSignal(bool, arguments=['error'])
    setSpinHeight = pyqtSignal(int, arguments=['val'])
    setHeightText = pyqtSignal(str, arguments=['msg'])
    setInvalidHeight = pyqtSignal(bool, arguments=['error'])
    setFilesMessage = pyqtSignal(str, arguments=['msg'])
    setToolTipWidthText = pyqtSignal(str, arguments=['width'])
    setToolTipHeightText = pyqtSignal(str, arguments=['height'])
    setKeepRatio = pyqtSignal(int, arguments=['checked'])
    setUsePercent = pyqtSignal(int, arguments=['checked'])
    setOverwrite = pyqtSignal(int, arguments=['checked'])
    setBusyIndicator = pyqtSignal(bool, arguments=['action'])
    closeApp = pyqtSignal()

    def __init__(self, images=[]):
        super().__init__()

        self.images = images

        self.keepRatio = True
        self.usePercent = False
        self.overWrite = False

        self.appendText = ".resized"

        self.tmpWidth = 0
        self.tmpHeight = 0
        self.nbrFiles = 0

        self.imageWidth = 0
        self.imageHeight = 0

        self.percent = 50

    def loadValues(self):
        self.setBusyIndicator.emit(True)

        for filename in self.images:
            QApplication.processEvents()

            tmpimg = Image(filename=filename)
            self.tmpWidth += tmpimg.width
            self.tmpHeight += tmpimg.height
            self.nbrFiles += 1

        self.imageWidth = int(self.tmpWidth/self.nbrFiles)
        self.imageHeight = int(self.tmpHeight/self.nbrFiles)

        self.setSpinWidth.emit(self.imageWidth)
        self.setWidthText.emit(str(self.imageWidth))
        print(str(self.imageWidth))
        self.setSpinHeight.emit(self.imageHeight)
        self.setHeightText.emit(str(self.imageHeight))
        print(str(self.imageHeight))

        self.setProgressMax.emit(self.nbrFiles)
        if self.nbrFiles > 1:
            self.setFilesMessage.emit("Resizing {0} Images".format(self.nbrFiles))
        else:
            self.setFilesMessage.emit("Resizing 1 Image")

        self.setAppendText.emit(self.appendText)
        self.setPercentage.emit(self.percent)

        self.setKeepRatio.emit(self.keepRatio)
        self.setUsePercent.emit(self.usePercent)
        self.setOverwrite.emit(self.overWrite)

        widthText = "Minimum width 32, Maximum width 32 768"
        heightText = "Minimum width 32, Maximum width 32 768"

        self.AspectRatio = 1
        self.side = 0
        if int(self.imageWidth) > int(self.imageHeight):
            self.side = 1
            self.AspectRatio = int(self.imageWidth) / int(self.imageHeight)
        elif int(self.imageWidth) < int(self.imageHeight):
            self.side = 2
            self.AspectRatio = int(self.imageHeight) / int(self.imageWidth)
        else:
            pass

        self.block = False

        self.setBusyIndicator.emit(False)

    @pyqtSlot(int)
    def onWidthChanged(self, width):
        self.imageWidth = width
        self.setWidthText.emit(str(self.imageWidth))

        if self.keepRatio:

            if self.side == 0:
                height = self.imageWidth
            elif self.side == 1:
                height = int(self.imageWidth/self.AspectRatio)
            elif self.side == 2:
                height = int(self.imageWidth * self.AspectRatio)

            if height < 32:
                height = 32

            self.setSpinHeight.emit(height)
            self.setHeightText.emit(str(height))

    @pyqtSlot(str)
    def onWidthTextChanged(self, width):
        if width == "" or int(width) < 32 or int(width) > 32768:
            self.setInvalidWidth.emit(True)
        else:
            self.setInvalidWidth.emit(False)

            self.imageWidth = int(width)
            self.setSpinWidth.emit(self.imageWidth)

            if self.keepRatio:

                if self.side == 0:
                    height = self.imageWidth
                elif self.side == 1:
                    height = int(self.imageWidth/self.AspectRatio)
                elif self.side == 2:
                    height = int(self.imageWidth * self.AspectRatio)

                if height < 32:
                    height = 32

                self.setSpinHeight.emit(height)
                self.setHeightText.emit(str(height))
                self.setInvalidHeight.emit(False)

        @pyqtSlot(int)
        def onHeightChanged(self, height):
            self.imageHeight = height
            self.setHeightText.emit(str(self.imageHeight))

            if self.keepRatio:

                if self.side == 0:
                    width = self.imageHeight
                elif self.side == 1:
                    width = int(self.imageHeight * self.AspectRatio)
                elif self.side == 2:
                    width = int(self.imageHeight / self.AspectRatio)

                if width < 32:
                    width = 32

                self.setSpinWidth.emit(width)
                self.setWidthText.emit(str(width))

    @pyqtSlot(str)
    def onHeightTextChanged(self, height):
        if height == "" or int(height) < 32 or int(height) > 32768:
            self.setInvalidHeight.emit(True)
        else:
            self.setInvalidHeight.emit(False)

            self.imageHeight = int(height)
            self.setSpinHeight.emit(self.imageHeight)

            if self.keepRatio:

                if self.side == 0:
                    width = self.imageHeight
                elif self.side == 1:
                    width = int(self.imageHeight * self.AspectRatio)
                elif self.side == 2:
                    width = int(self.imageHeight / self.AspectRatio)

                if width < 32:
                    width = 32

                self.setSpinWidth.emit(width)
                self.setWidthText.emit(str(width))
                self.setInvalidWidth.emit(False)

    @pyqtSlot(int)
    def onPercentChanged(self, percent):
        self.percent = percent

    @pyqtSlot(bool)
    def onRatioToggled(self, checked):
        self.keepRatio = checked
        # if checked:
        #     self.keepRatio = checked
        # else:
        #     self.keepRatio = Qt.Unchecked

    @pyqtSlot(bool)
    def onUsePercentToggled(self, checked):
        self.usePercent =checked
        # if checked:
        #     self.usePercent =checked
        # else:
        #     self.usePercent = Qt.Unchecked

    @pyqtSlot(bool)
    def onOverwriteToggled(self, checked):
        self.overWrite = checked
        # if checked:
        #     self.overWrite = checked
        # else:
        #     self.overWrite = Qt.Unchecked

    @pyqtSlot(str)
    def onTextChanged(self, text):
        self.appendText = text

    @pyqtSlot()
    def onProcess(self):
        self.resizeImages()

        self.closeApp.emit()

    def closeEvent(self, event):
        pass

    def resizeImages(self):
        cnt = 0

        for filename in self.images:
            QApplication.processEvents()

            tmpimg = Image(filename=filename)

            ptr = filename.rfind(".")
            filext = filename[ptr:]

            save_filename = filename[:(ptr)] + self.appendText + filext

            if self.usePercent:
                newWidth = int(tmpimg.width * int(self.percent)/100)
                newHeight = int(tmpimg.height * int(self.percent)/100)
            else:
                newWidth = int(self.imageWidth)
                newHeight = int(self.imageHeight)

            tmpimg.resize(newWidth, newHeight)

            if self.overWrite:
                if Path(filename).is_file():
                    Path(filename).unlink()

                tmpimg.save(filename=filename)
            else:
                if Path(save_filename).is_file():
                    Path(save_filename).unlink()

                tmpimg.save(filename=save_filename)

            cnt += 1
            self.updateProgress.emit(cnt)


def main():
    app = QApplication(sys.argv)

    images = []
    try:
        images = sys.argv[1:]
    except:
        pass

    if len(images) > 0:

        app.setOrganizationName("Some Company")
        app.setOrganizationDomain("somecompany.com")
        app.setApplicationName("Amazing Application")

        engine = QQmlApplicationEngine()
        engine = QQmlApplicationEngine()

        engine.quit.connect(app.quit)

        qmlFile= os.path.dirname(os.path.realpath(__file__)) + "/main.qml"
        engine.load(qmlFile)

        # Define our backend object, which we pass to QML.
        backend = Backend(images)
        engine.rootObjects()[0].setProperty('backend', backend)
        backend.loadValues()
        sys.exit(app.exec_())


if __name__ == "__main__":
    main()
