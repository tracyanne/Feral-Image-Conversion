import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    anchors.fill: parent

    Rectangle {
        id: toolTipRectangle
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.bottom
        width: toolTipText.width + 4
        height: toolTipText.height + 4
        z: 200
        visible: true
        color: "#ffffaa"
        border.color: "#0a0a0a"
        Text {
            id: toolTipText
            text: toolTip
            color: "black"
            anchors.centerIn: parent
        }
    }
}
