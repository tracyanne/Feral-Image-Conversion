import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    id: resizeWindow
    width: 305
    height: 340
    visible: true
    title: qsTr("Resize selected Image Files")
    flags: Qt.FramelessWindowHint | Qt.Window | Qt.Tool| Qt.WindowStaysOnTopHint | Qt.ApplicationModal | Qt.WindowModal

    property int spinwidthvalue
    property string textwidthvalue
    property int spinheightvalue
    property string textheightvalue

    property string normalcolor

    property int spinpercentagevalue

    property bool chkKeepRatiostate
    property bool chkUsePercentstate
    property bool chkOverwritestate

    property QtObject backend
    //property Component itemtooltip: ItemToolTip /*{}*/

    Component {
        id: itemToolTip; Item {
            anchors.fill: parent
            property string toolTip: "Minimum value 32\nMaximum value 32768"
            property bool showToolTip: false
            Rectangle {
                id: toolTipRectangle
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.bottom
                width: toolTipText.width + 8
                height: toolTipText.height + 4
                z: 200
                visible: showToolTip
                color: "#ffffaa"
                border.color: "#0a0a0a"
                Text {
                    id: toolTipText
                    text: toolTip
                    color: "black"
                    anchors.centerIn: parent
                }
                Behavior on opacity {
                    PropertyAnimation {
                        easing.type: Easing.InOutQuad
                        duration: 250
                    }
                }
            }
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                onEntered: showTimer.start()
                onExited: { showToolTip = false; showTimer.stop(); }
                onClicked: { showToolTip = false; showTimer.stop();}
                hoverEnabled: true
            }
            Timer {
                id: showTimer
                interval: 250
                onTriggered: showToolTip = true;
            }
        }
    }

    BusyIndicator {
        id: busyIndicator
        x: 133
        y: 150
        z: 5
        visible: false
        running: false
    }

    Label {
        id: label1
        x: 10
        y: 10
        width: 115
        height: 32
        visible: true
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Width")
    }

    SpinBox {
        id: spnWidth
        x: 125
        y: 10
        height: 32
        width: 90
        from: 32
        to: 32768
        editable: true
        value: spinwidthvalue
        onValueModified: backend.onWidthChanged(spnWidth.value)
    }

    TextField {
        id: textWidth
        x: 125
        y: 10
        width: 70
        height: 32
        text: textwidthvalue
        z: 2
        validator: IntValidator{bottom: 32; top: 32768;}
        onTextEdited: backend.onWidthTextChanged(textWidth.text)
    }

    Button {
        id: widthHelp
        x: 215
        y: 18
        z: 200
        width: 16
        height: 16
        text: "?"
        Loader {
            sourceComponent: itemToolTip
            anchors.fill: parent
            z: 200
        }
    }

    Label {
        id: label2
        x: 10
        y: 43
        width: 115
        height: 32
        visible: true
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Keep Aspect Ratio")
    }

    CheckBox {
        id: chkKeepRatio
        x: 125
        y: 45
        height: 32
        rotation: 0
        tristate: false
        checked: chkKeepRatiostate
        onToggled: backend.onRatioToggled(chkKeepRatio.checked)
    }

    Label {
        id: label3
        x: 10
        y: 77
        width: 115
        height: 32
        visible: true
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Height")
    }

    SpinBox {
        id: spnHeight
        x: 125
        y: 77
        height: 32
        width: 90
        from: 32
        to: 32768
        editable: true
        value: spinheightvalue
        onValueModified: backend.onHeightChanged(spnHeight.value)
    }

    TextField {
        id: textHeight
        x: 125
        y: 77
        width: 70
        height: 32
        text: textheightvalue
        z: 2
        validator: IntValidator{bottom: 32; top: 32768;}
        onTextEdited: backend.onHeightTextChanged(textHeight.text)
    }

    Button {
        id: heightHelp
        x: 215
        y: 85
        z: 200
        width: 16
        height: 16
        text: "?"
        Loader {
            sourceComponent: itemToolTip
            anchors.fill: parent
            z: 200
        }
    }

    Label {
        id: label4
        x: 10
        y: 120
        width: 115
        height: 32
        visible: true
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Resize by")
    }

    SpinBox {
        id: spnPercent
        x: 125
        y: 120
        height: 32
        width: 90
        from: 1
        to: 100
        editable: true
        value: spinpercentagevalue
        onValueModified: backend.onPercentChanged(spnPercent.value)
    }

    CheckBox {
        id: chkUsePercent
        x: 221
        y: 123
        width: 80
        height: 26
        visible: true
        text: qsTr("Percent")
        tristate: false
        checked: chkUsePercentstate
        onToggled: backend.onUsePercentToggled(chkUsePercent.checked)
    }


    Label {
        id: label5
        x: 10
        y: 150
        width: 115
        height: 32
        visible: true
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Overwrite original")
    }

    CheckBox {
        id: chkOverwrite
        x: 125
        y: 150
        height: 32
        rotation: 0
        visible: true
        tristate: false
        checked: chkOverwritestate
        onToggled: backend.onOverwriteToggled(chkOverwrite.checked)
    }

    Label {
        id: label6
        x: 10
        y: 180
        width: 115
        height: 32
        visible: true
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Append with")
    }

    TextField {
        id: txtAppendText
        x: 125
        y: 180
        height: 32
        width: 120
        text: ""
        onTextEdited: backend.onTextChanged(txtAppendText.text)
    }

    Label {
        id: label7
        x: 10
        y: 215
        width: 286
        height: 32
        visible: true
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Resizing 3000 Files")
    }

    ProgressBar {
        id: resizeProgress
        x: 10
        y: 250
        width: 286
        height: 24
        from: 0.0
        to: 10.0
        value: 0.0
    }

    Button {
        id: btnProcess
        x: 120
        y: 295
        text: qsTr("Resize")
        onClicked: backend.onProcess()
    }

    Button {
        id: btnCancel
        x: 211
        y: 295
        text: qsTr("Cancel")
        onClicked: resizeWindow.close()
    }

    Connections {
        target: backend

        function onSetBusyIndicator(action) {
            busyIndicator.visible = action
            busyIndicator.running = action
            normalcolor = textWidth.color
        }

        function onCloseApp() {
            resizeWindow.close()
        }

        function onSetAppendText(msg) {
            txtAppendText.text = msg
        }

        function onSetSpinWidth(val) {
            spinwidthvalue = val
        }

        function onSetWidthText(msg) {
            textwidthvalue = msg
        }

        function onSetInvalidWidth(error) {
            if (error) {
                textWidth.color = "red";
            } else {
                textWidth.color = normalcolor;
            }
        }

        function onSetSpinHeight(val) {
            spinheightvalue = val
        }

        function onSetHeightText(msg) {
            textheightvalue = msg
        }

        function onSetInvalidHeight(error) {
            if (error) {
                textHeight.color = "red";
            } else {
                textHeight.color = normalcolor;
            }
        }

        function onSetFilesMessage(msg) {
            label7.text = msg
        }

        function onSetProgressMax(maxval) {
            resizeProgress.to = maxval
        }

        function onUpdateProgress(counter) {
            resizeProgress.value = counter
        }

        function onSetPercentage(val) {
            spinpercentagevalue = val
        }

        function onSetKeepRatio(checked) {
            chkKeepRatiostate = checked
        }

        function onSetUsePercent(checked) {
            chkUsePercentstate = checked
        }

        function onSetOverwrite(checked) {
            chkOverwritestate = checked
        }
    }
}
