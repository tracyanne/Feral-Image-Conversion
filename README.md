# Feral-Image-Conversion
KDE Dolphin Service Menu Apps

Download the Source Code as a .zip or .tar.gz and extract it to a Directory


Requires PyQt5

To install requirements...

Manjaro : pamac install pyqt5

Debian KDE, Kubuntu : apt install python3-pyqt5*

Fedora : yum install pyqt5


To install the Apps

Create the Directory ~/.local/share/kservices5 and the directory ~/.local/share/kservices5/ServiceMenus, if they do not already exist.

Copy the directorys QMLReformat, QMLResize, QMLRotate to ~/.local/share/kservices5/

Copy the .desktop files convertImages.desktop, resizeImages.desktop, rotateImages.desktop to ~/.local/share/kservices5/ServiceMenus/

The apps should now become available in the Dolphin Context Menu.

To enable these apps... in Dolphin select from the menu Settings -> Configure Dolphin -> Context Menu.

scroll through the Context Menu items and enable the following items..

Change Image Format
Resize Images
Rotate Images

Make sure the python files are executable

chmod +x ~/.local/share/kservices5/QMLReformat/reformat/reformat.py

chmod +x ~/.local/share/kservices5/QMLResize/resize/resize.py

chmod +x ~/.local/share/kservices5/QMLRotate/rotate/rotate.py

These items will be available whenever you right mouse click over any image.


OR

After making sure the requirements are met - run the installer.

Open a Terminal

make sure the installer has it's execute flag set.

chmod +x install.sh

run the installer

./install.sh
