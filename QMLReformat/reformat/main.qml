import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15


ApplicationWindow {
    id: convertWindow
    width: 305
    height: 208
    visible: true
    title: qsTr("Change selected Images Format")
    modality: Qt.WindowModal
    flags: Qt.FramelessWindowHint | Qt.Window | Qt.Tool | Qt.WindowStaysOnTopHint | Qt.ApplicationModal | Qt.WindowModal

    property int proxyrotationvalue
    property string textrotationvalue

    property QtObject backend
    property string selectedFormat

    BusyIndicator {
        id: busyIndicator
        x: 133
        y: 150
        z: 2
        visible: false
        running: false
    }


    Label {
        id: label0
        x: 10
        y: 10
        width: 135
        height: 32
        visible: true
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Required Image Type")
    }

    ComboBox {
        id: imageTypeSelection
        x: 145
        y: 10
        width: 135
        height: 32
        textRole: "text"
        valueRole: "value"
        // When an item is selected, update the backend.
        onActivated: backend.onchangeSelectedFormat(currentValue)
        // Set the initial currentIndex to the value stored in the backend.
        Component.onCompleted: currentIndex = indexOfValue(selectedFormat)
        model: [
            { value: "", text: qsTr("Select") },
            { value: "jpeg", text: qsTr("JPEG(.jpeg or .jpg)") },
            { value: "tiff", text: qsTr("TIFF(.tiff or .tif)") },
            { value: "png", text: qsTr("PNG(.png)") },
            { value: "gif", text: qsTr(" GIF(.gif)") },
            { value: "eps", text: qsTr("EPS(.eps)") },
            { value: "bmp", text: qsTr("Bitmap(.bmp)") }
        ]
    }

    Label {
        id: label6
        x: 10
        y: 74
        width: 115
        height: 32
        visible: true
        verticalAlignment: Text.AlignVCenter
        text: qsTr("Append with")
    }

    TextField {
        id: txtAppendText
        x: 125
        y: 74
        height: 32
        width: 120
        text: ""
        onTextEdited: backend.onTextChanged(txtAppendText.text)
    }

    Label {
        id: label2
        x: 10
        y: 106
        width: 285
        height: 32
        text: qsTr("Converting 3000 Images")
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    ProgressBar {
        id: rotateProgress
        x: 10
        y: 138
        width: 285
        height: 24
        from: 0.0
        to: 10.0
        value: 0.0
    }

    Button {
        id: btnProcess
        x: 120
        y: 163
        text: qsTr("Convert")
        onClicked: backend.onProcess()
    }

    Button {
        id: btnCancel
        x: 211
        y: 163
        text: qsTr("Cancel")
        onClicked: convertWindow.close()
    }


    Connections {
        target: backend

        function onSetBusyIndicator(action) {
            busyIndicator.visible = action
            busyIndicator.running = action
        }

        function onCloseApp() {
            convertWindow.close()
        }

        function onSetSelectedFormat(msg) {
            selectedFormat = msg
        }

        function onSetAppendText(msg) {
            txtAppendText.text = msg
        }

        function onSetFilesMessage(msg) {
            label2.text = msg
        }

        function onSetProgressMax(maxval) {
            rotateProgress.to = maxval
        }

        function onUpdateProgress(counter) {
            rotateProgress.value = counter
        }
    }
}
