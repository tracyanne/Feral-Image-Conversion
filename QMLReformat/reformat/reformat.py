#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 27 08:32:00 2023

@author: tracy barlow
"""

# #This program is free software; you can redistribute it and/or modify it under
# #the terms of the GNU General Public License as published by
# #the Free Software Foundation; either version 2 of the License,
# #or (at your option) any later version.
# #
# #This program is distributed in the hope that it will be useful,
# #but WITHOUT ANY WARRANTY; without even the implied warranty
# #of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# #See the GNU General Public License for more details.
# #
# #You should have received a copy of the GNU General Public License along
# #with this program; if not, write to the Free Software Foundation, Inc.,
# #59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# This Python file uses the following encoding: utf-8
import sys
from pathlib import Path

from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import pyqtSignal, QObject, pyqtSlot, QUrl, Qt
from PyQt5.QtWidgets import QApplication

from wand.image import Image
import os


class Backend(QObject):

    setAppendText = pyqtSignal(str, arguments=['msg'])
    setProgressMax = pyqtSignal(float, arguments=['maxval'])
    updateProgress = pyqtSignal(int, arguments=['counter'])
    setFilesMessage = pyqtSignal(str, arguments=['msg'])
    setOverwrite = pyqtSignal(int, arguments=['checked'])
    setBusyIndicator = pyqtSignal(bool, arguments=['action'])
    setSelectedFormat = pyqtSignal(str, arguments=['msg'])
    closeApp = pyqtSignal()

    def __init__(self, images=[]):
        super().__init__()

        self.images = images

        self.rotation = 0
        self.appendText = ".converted"
        self.selectedFormat = ""

        self.tmpWidth = 0
        self.tmpHeight = 0
        self.nbrFiles = 0

        self.imageWidth = 0
        self.imageHeight = 0

    def showEvent(self, event):
        print("Showing")

    def loadValues(self):
        self.setBusyIndicator.emit(True)

        for filename in self.images:
            QApplication.processEvents()
            self.nbrFiles += 1

        self.setProgressMax.emit(self.nbrFiles)
        if self.nbrFiles > 1:
            self.setFilesMessage.emit("Converting {0} Images".format(self.nbrFiles))
        else:
            self.setFilesMessage.emit("Converting 1 Image")

        self.setAppendText.emit(self.appendText)
        self.setSelectedFormat.emit("")
        self.setBusyIndicator.emit(False)

    @pyqtSlot(str)
    def onTextChanged(self, text):
        self.appendText = text

    @pyqtSlot(str)
    def onchangeSelectedFormat(self, value):
        self.selectedFormat = value
        print(self.selectedFormat, value)

    @pyqtSlot()
    def onProcess(self):
        self.convertImages()

        self.closeApp.emit()

    def closeEvent(self, event):
        pass

    def convertImages(self):
        if self.selectedFormat == "":
            pass
        else:
            for imageName in self.images:
                with Image(filename=imageName) as tmpimg:
                    # Change format in python using format property
                    tmpimg.format = self.selectedFormat
                    ptr = imageName.rfind(".")
                    save_filename = imageName[:(ptr)] + self.appendText + "." + self.selectedFormat

                    # Save final image
                    if Path(save_filename).is_file():
                        Path(save_filename).unlink()

                    tmpimg.save(filename=save_filename)

def main():
    app = QApplication(sys.argv)

    images = []
    try:
        images = sys.argv[1:]
    except:
        pass

    if len(images) > 0:

        app.setOrganizationName("Some Company")
        app.setOrganizationDomain("somecompany.com")
        app.setApplicationName("Amazing Application")

        engine = QQmlApplicationEngine()
        engine = QQmlApplicationEngine()

        engine.quit.connect(app.quit)

        qmlFile= os.path.dirname(os.path.realpath(__file__)) + "/main.qml"
        engine.load(qmlFile)

        # Define our backend object, which we pass to QML.
        backend = Backend(images)
        engine.rootObjects()[0].setProperty('backend', backend)
        backend.loadValues()
        sys.exit(app.exec_())


if __name__ == "__main__":
    main()
